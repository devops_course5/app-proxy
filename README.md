# App Proxy

NGINX proxy app

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to liston on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
